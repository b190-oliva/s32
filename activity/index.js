



const http = require("http");

const port = 4000;

const server = http.createServer((request,response)=>{
	
	// route for returning all items upon receiving a GET method request
	if(request.url === "/" && request.method === "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write(`Welcome to Booking System`);
		response.end();
	}
	else if(request.url === "/profile" && request.method === "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write(`Welcome to your profile!`);
		response.end();
	}
	else if(request.url === "/courses" && request.method === "GET"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write(`Here’s our courses available:`);
		response.end();
	}
	else if(request.url === "/addcourse" && request.method === "POST"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write(`Add a course to our resources`);
		response.end();
	}
	else if(request.url === "/updatecourse" && request.method === "PUT"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write(`Update a course to our resources`);
		response.end();
	}
	else if(request.url === "/archivecourses" && request.method === "DELETE"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.write(`Archive courses to our resources`);
		response.end();
	}
	else{
		response.writeHead(404, {"Content-Type":"text/plain"});
		response.write(`Page not found!`);
		response.end();
	}

});

server.listen(port);

console.log(`Server now running at port: ${port}`);