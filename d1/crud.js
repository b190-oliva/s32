


const http = require("http");

const port = 4000;

//Mock Database

let directory = [
{
	"name": "Brandon",
	"email": "brandon@mail.com"
},
{
	"name": "jobert",
	"email": "jobert@mail.com"
}
];

const server = http.createServer((request,response)=>{
	
	// route for returning all items upon receiving a GET method request
	if(request.url === "/users" && request.method === "GET"){
		// request the "/users" path and "GET" the information
		response.writeHead(200, {"Content-Type":"application/json"});
		// set the status code to 200, meaning OK
		// sets the response output to JSON data type
		response.write(JSON.stringify(directory));
		//response.write() - write any specified string
		// JSON.stringify is needed as the needed data type is string.
		response.end();
	};

	if(request.url === "/users" && request.method === "POST"){
		/*
			A stream is a sequence of data
			data is received from the client and is processed in the data stream
			the information provided from the request object enters a sequence called "data" the code below will be triggered
			- data step: this reads the data stream and process it as the request body
		*/
		let requestBody = "";
		request.on("data",function(data){
				requestBody += data;
			});
		//end step, only runs after the request has completely been sent
		request.on("end", function(){
			//checks if at this point, the requestBody if off data type STRING
			//we need this to be of data type JSON to access it's properties
			console.log(typeof requestBody);
			// this converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// creating new object representing the new mock database

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			};

			// adds the new user into the mock database
			directory.push(newUser);
			console.log(directory);
			response.writeHead(200, {"Content-Type":"application/json"});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	};
});

server.listen(port);

console.log(`Server now running at port: ${port}`);