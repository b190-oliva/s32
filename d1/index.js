const http = require("http");

const port = 4000;

const server = http.createServer((request,response)=>{
	// HTTP method can be accessed via "method" property inside 
	if(request.url === "/items" && request.method === "GET"){
		response.writeHead(200,{"Content-Type":"text/plain"});
		response.end(`Data retrieved from the database`);
	}
	//post request
	// "POST" means that we will be adding/creating information
	if(request.url === "/items" && request.method === "POST"){
		response.writeHead(200,{"Content-Type":"text/plain"});
		response.end(`Data to be sent to the database`);
	};

});

// GET method is one of the https methods that we will be using from this point.
// GET method means that we will be retrieving or reading information

server.listen(port);

console.log(`Server now running at port: ${port}`);

/*
	postman - since the node.js application that we are building is a backend application, and there is no frontend application yet to process the request, we will using postman to process the request.

*/

// npx kill-port <port> - used to terminate the port activity